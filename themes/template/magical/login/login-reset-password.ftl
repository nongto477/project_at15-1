<#import "template.ftl" as layout>
<@layout.registrationLayout layoutClass="form-login form-forgot-password" displayMessage=!messagesPerField.existsError('username'); section>
    <#if section = "header">
        <h3 class="headline">
            ${msg("emailForgotTitle")}
        </h3>
        <p>${msg("emailInstruction")}</p>
    <#elseif section = "form">
        <form class="form-forgot" action="${url.loginAction}" method="post">
            <div class="form-group">
                <input class="form-control form-input" type="text" id="username" name="username" autofocus value="${(auth.attemptedUsername!'')}" 
                    aria-invalid="<#if messagesPerField.existsError('username')>true</#if>" 
                    placeholder="<#if !realm.loginWithEmailAllowed>${msg('username')}<#elseif !realm.registrationEmailAsUsername>${msg('usernameOrEmail')}<#else>${msg('email')}</#if>">
                <#if messagesPerField.existsError('username')>
                    <span id="input-error-username" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                ${kcSanitize(messagesPerField.get('username'))?no_esc}
                    </span>
                </#if>
            </div>
            <button type="submit" class="btn btn-login">${msg("doSend")}</button>
        </form>
        <a href="${url.loginUrl}" class="text-forgot">${kcSanitize(msg("backToLogin"))?no_esc}</a>
    </#if>
</@layout.registrationLayout>
