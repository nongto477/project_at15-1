#!/bin/bash

# get pod name keycloak
kubectl get pod -n magicak | grep keycloak | grep -ve keycloak-mysql | awk '{print $1}' > /tmp/keycloak-pod-name.txt

for i in `cat /tmp/keycloak-pod-name.txt`
do
    echo "Syncing themes for pod $i"
    kubectl cp -n magicak ./themes/template/magical $i:/opt/jboss/keycloak/themes
done

