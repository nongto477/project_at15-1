import Vue from 'vue';

// axios
import axios from 'axios';

const axiosIns = axios.create({
  // You can add your headers here
  // ================================
  // baseURL: 'https://magicakweb.dev/api/',
  // timeout: 1000,
  headers: {
    // 'Access-Control-Allow-Origin': '*',
    // 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, PATCH, OPTIONS',
    // 'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT',
    // 'Access-Control-Allow-Credentials': 'true',
    // 'Access-Control-Allow-Headers': 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization,X-Client-Identifier',
    // 'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Custom-Header',
  },
});
// axiosIns.defaults.crossDomain = true
// const axiosSSO = axios.create({
//   // You can add your headers here
//   // ================================
//   baseURL: 'https://magicakweb.dev/api/',
//   // timeout: 1000,
//   // headers: {'X-Custom-Header': 'foobar'}
// })

Vue.prototype.$http = axiosIns;

export default axiosIns;
