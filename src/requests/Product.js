import requestApi from '@Service/RequestApiMiddleware';

const dataProducts          = '/products';
const product          = '/product';
const dataProductCategory = '/product-categories';

export default {
  getProduct() {
    return requestApi.get(`${dataProducts}`);
  },
  getProductDetail(id) {
    return requestApi.get(`/product/${id}`);
  },

  getProductCategory() {
    return requestApi.get(`${dataProductCategory}`);
  },
  
};
