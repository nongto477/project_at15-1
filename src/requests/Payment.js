import requestApi from '@Service/RequestApiMiddleware';

const createStripeOrder          = '/create-stripe-order';
const createPaypalOrder          = '/create-paypal-order';
const capturePaypalOrder          = '/capture-paypal-order';

export default {
    createStripeOrder(data) {
    return requestApi.post(`${createStripeOrder}`,data);
  },
  createPaypalOrder(){
    return requestApi.get(`${createPaypalOrder}`);
  },
  capturePaypalOrder(data){
    return requestApi.post(`${capturePaypalOrder}`,data);
  }
};
