
import ProductRequest from './Product';
import CartRequest from './Cart';
import PaymentRequest from './Payment';
import OrderRequest from './Order';

const repositories = {
  product: ProductRequest,
  cart: CartRequest,
  payment: PaymentRequest,
  order: OrderRequest,
};

export const RequestFactory = {
  get: name => repositories[name]
};
