import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  // base: process.env.BASE_URL,
  // base: `${process.env.BASE_URL}/admin/`,
  base: '/',
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  routes: [
    {
      path: '/',
      name: 'index',
      component: () => import('@/views/Home.vue'),
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/cart',
      name: 'cart',
      component: () => import('@/views/Cart.vue'),
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/order',
      name: 'order',
      component: () => import('@/views/Order.vue'),
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/check-out',
      name: 'check-out',
      component: () => import('@/views/Checkout.vue'),
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/book/:id',
      name: 'book-detail',
      component: () => import('@/views/BookDetail.vue'),
      meta: {
        requiresAuth: true,
      },
    },
  ],
});

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

router.beforeEach(async (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // We wait for Keycloak init, then we can call all methods safely
    while (router.app.$keycloak.createLoginUrl === null) {
      // eslint-disable-next-line no-await-in-loop
      await sleep(100);
    }
    if (router.app.$keycloak.authenticated) {
      next();
    } else {
      const loginUrl = router.app.$keycloak.createLoginUrl();
      window.location.replace(loginUrl);
    }
  } else {
    next();
  }
});

// ? For splash screen
// Remove afterEach hook if you are not using splash screen
router.afterEach(() => {
  // Remove initial loading
  const appLoading = document.getElementById('loading-bg');
  if (appLoading) {
    appLoading.style.display = 'none';
  }
});

export default router;
